import Vue from 'vue'
import App from './App.vue'

import Langages from './components/langages/langages.vue'
Vue.component('Langages', Langages);

new Vue({
  el: '#app',
  render: h => h(App)
});

Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})
